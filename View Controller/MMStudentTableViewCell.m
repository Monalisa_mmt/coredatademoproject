//
//  MMStudentTableViewCell.m
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import "MMStudentTableViewCell.h"

@implementation MMStudentTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.namelabel = [[UILabel alloc] init];
    self.namelabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.namelabel setBackgroundColor:[UIColor colorWithRed:203.0/255.0f
                                                              green:217.0/255.0f
                                                               blue:242.0/255.0f
                                                              alpha:1.0]];
    [self addSubview:self.namelabel];
    
    self.contactLabel= [[UILabel alloc] init];
    self.contactLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contactLabel setBackgroundColor:[UIColor colorWithRed:203.0/255.0f
                                                       green:217.0/255.0f
                                                        blue:242.0/255.0f
                                                       alpha:1.0]];
    [self addSubview:self.contactLabel];
    
    self.emailID= [[UILabel alloc] init];
    self.emailID.translatesAutoresizingMaskIntoConstraints = NO;
    [self.emailID setBackgroundColor:[UIColor colorWithRed:203.0/255.0f
                                                          green:217.0/255.0f
                                                           blue:242.0/255.0f
                                                          alpha:1.0]];
    [self addSubview:self.emailID];
    
    NSDictionary *views = [NSDictionary dictionaryWithObjectsAndKeys:self.namelabel, @"nameLabel", self.contactLabel, @"contactLabel", self.emailID, @"emailID",nil];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[nameLabel(30)]-3-[contactLabel]-5-[emailID]-5-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[nameLabel]-5-|"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:views]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.contactLabel
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.namelabel
                                                     attribute:NSLayoutAttributeLeading
                                                    multiplier:1
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.emailID
                                                     attribute:NSLayoutAttributeLeading
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.namelabel
                                                     attribute:NSLayoutAttributeLeading
                                                    multiplier:1
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.contactLabel
                                                     attribute:NSLayoutAttributeTrailing
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.namelabel
                                                     attribute:NSLayoutAttributeTrailing
                                                    multiplier:1
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.emailID
                                                     attribute:NSLayoutAttributeTrailing
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.namelabel
                                                     attribute:NSLayoutAttributeTrailing
                                                    multiplier:1
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.contactLabel
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.namelabel
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1
                                                      constant:0.0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.emailID
                                                     attribute:NSLayoutAttributeHeight
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:self.namelabel
                                                     attribute:NSLayoutAttributeHeight
                                                    multiplier:1
                                                      constant:0.0]];
    return self;
}

@end
