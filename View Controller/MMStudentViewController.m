//
//  MMStudentViewController.m
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import "MMStudentViewController.h"
#import "MMStudentInformationViewController.h"
#import "Student+CoreDataProperties.h"
#import "MMCoreDataHandler.h"

@interface MMStudentViewController ()

@property (strong, nonatomic)UIAlertController *alertController;

@end


@implementation MMStudentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   
//    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.heading = [[UILabel alloc] init];
    self.heading.translatesAutoresizingMaskIntoConstraints = NO;
    self.heading.text = @"Enter student details :-";
    [self.view addSubview:self.heading];
    
    self.navigationItem.title = @"Student Information";
    self.nameLabel = [[UITextField alloc] init];
    self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.nameLabel setPlaceholder:@"Name" ];
    [self.nameLabel setBorderStyle:UITextBorderStyleRoundedRect];
    [self.nameLabel setBackgroundColor:[UIColor colorWithRed:203.0/255.0f
                                                       green:217.0/255.0f
                                                        blue:242.0/255.0f
                                                       alpha:1.0]];
    [self.view addSubview:self.nameLabel];
        
    self.contactNoLabel = [[UITextField alloc] init];
    self.contactNoLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contactNoLabel setPlaceholder:@"ContactNo" ];
    [self.contactNoLabel setBorderStyle:UITextBorderStyleRoundedRect];
    [self.contactNoLabel setKeyboardType:UIKeyboardTypeNumberPad];
    [self.contactNoLabel setBackgroundColor:[UIColor colorWithRed:203.0/255.0f
                                                            green:217.0/255.0f
                                                             blue:242.0/255.0f
                                                            alpha:1.0]];
    [self.view addSubview:self.contactNoLabel];
    
    self.emailID = [[UITextField alloc] init];
    self.emailID.translatesAutoresizingMaskIntoConstraints = NO;
    [self.emailID setPlaceholder:@"Email Id" ];
    [self.emailID setBorderStyle:UITextBorderStyleRoundedRect];
    [self.emailID setBackgroundColor:[UIColor colorWithRed:203.0/255.0f
                                                     green:217.0/255.0f
                                                      blue:242.0/255.0f
                                                     alpha:1.0]];
    [self.view addSubview:self.emailID];
        
    self.submitButton = [[UIButton alloc] init];
    self.submitButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.submitButton addTarget:self action:@selector(submitButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.submitButton.backgroundColor=[UIColor blueColor];
    [self.submitButton setTitle:@"Store" forState:UIControlStateNormal];
    [self.submitButton setBackgroundColor:[UIColor colorWithRed:62.0f/255.0f
                                                     green:69.0f/255.0f
                                                      blue:115.0f/255.0f
                                                     alpha:1.0]];
    [self.view addSubview:self.submitButton];
        
    self.showButton = [[UIButton alloc] init];
    self.showButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.showButton addTarget:self action:@selector(showButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    self.showButton.backgroundColor=[UIColor blueColor];
    [self.showButton setTitle:@"Show" forState:UIControlStateNormal];
    [self.showButton setBackgroundColor:[UIColor colorWithRed:62.0f/255.0f
                                                          green:69.0f/255.0f
                                                           blue:115.0f/255.0f
                                                          alpha:1.0]];
    [self.view addSubview:self.showButton];
        
    [self setUpLayouts];
}
-(void)setUpLayouts
{
    NSDictionary *views = [NSDictionary dictionaryWithObjectsAndKeys:self.nameLabel, @"nameLabel",
                               self.contactNoLabel, @"contactNoLabel",
                               self.emailID, @"emailID",
                               self.submitButton, @"submitButton",
                               self.showButton, @"showButton",
                                self.heading, @"heading", nil];
        
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[heading(30)]-10-[nameLabel(40)]-5-[contactNoLabel]-5-[emailID]-50-[submitButton(30)]"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[nameLabel]-10-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:views]];
    
    [self. view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[submitButton(60)]-20-|"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:views]];
    
    [self. view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[showButton(60)]"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:views]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.heading
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.nameLabel
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.heading
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.nameLabel
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]];
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contactNoLabel
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.nameLabel
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1
                                                               constant:0]];
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.emailID
                                                              attribute:NSLayoutAttributeLeading
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.nameLabel
                                                              attribute:NSLayoutAttributeLeading
                                                             multiplier:1
                                                               constant:0]];
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contactNoLabel
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.nameLabel
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1
                                                               constant:0]];
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.emailID
                                                              attribute:NSLayoutAttributeTrailing
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.nameLabel
                                                              attribute:NSLayoutAttributeTrailing
                                                             multiplier:1
                                                               constant:0]];
        
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contactNoLabel
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.nameLabel
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1
                                                               constant:0]];
        
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.emailID
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.nameLabel
                                                              attribute:NSLayoutAttributeHeight
                                                             multiplier:1
                                                               constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.showButton
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.submitButton
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1
                                                           constant:0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.showButton
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.submitButton
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0]];
}

-(void)showButtonClicked:(id)sender
{
    MMStudentInformationViewController *mmStudentInformationViewController = [[MMStudentInformationViewController alloc] init];
    [self.navigationController pushViewController:mmStudentInformationViewController animated:YES];
}



-(void)submitButtonClicked:(id)sender
{
    NSString *nameString = self.nameLabel.text;
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet];
    NSString *filtered = [[nameString componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    if(![nameString isEqualToString:filtered])
    {
        [self validation:@"Only characters allowed in the name."];
        return;
    }
    NSString *contactString = self.contactNoLabel.text;
    NSString *emailIDString = self.emailID.text;
    
    if([nameString length] == 0 || [contactString length] == 0 || [emailIDString length] == 0)
    {
        [self validation:@"Cannot leave text field empty"];
        return;
    }
   
    if([contactString length] < 10)
    {
        [self validation:@"Enter valid Contact Number."];
        self.contactNoLabel.text = @"";
        return;
    }
    NSString *emailRegex = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if(![emailTest evaluateWithObject:emailIDString])
    {
        [self validation:@"Enter valid email ID"];
        self.emailID.text = @"";
        return;
    }
        
    [[MMCoreDataHandler sharedManager] storeStudentDetails:nameString second:contactString  third:emailIDString];
    self.nameLabel.text = @"";
    self.contactNoLabel.text = @"";
    self.emailID.text = @"";
    
}

-(void)validation:(NSString *) messageString
{
    self.alertController = [UIAlertController alertControllerWithTitle:@"Invalid!"
                                                               message:messageString
                                                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                                                    {
                                                    }];
    [self.alertController addAction:okButton];
    [self presentViewController:self.alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
