//
//  MMStudentViewController.h
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface MMStudentViewController : UIViewController

@property (strong, nonatomic)UITextField *nameLabel;
@property (strong, nonatomic)UITextField *contactNoLabel;
@property (strong, nonatomic)UITextField *emailID;
@property (strong, nonatomic)UIButton *submitButton;
@property (strong, nonatomic)AppDelegate *appDelegate;
@property (strong, nonatomic)UIButton *showButton;
@property (strong, nonatomic)NSManagedObjectContext *context;
@property (strong, nonatomic) UILabel *heading;

@end
