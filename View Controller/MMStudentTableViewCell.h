//
//  MMStudentTableViewCell.h
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMStudentTableViewCell : UITableViewCell
@property (strong, nonatomic)UILabel *namelabel;
@property (strong, nonatomic)UILabel *contactLabel;
@property (strong, nonatomic)UILabel *emailID;

@end
