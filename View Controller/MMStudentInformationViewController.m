//
//  MMStudentInformationViewController.m
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import "MMStudentInformationViewController.h"
#import "MMStudentTableViewCell.h"
#import "Student+CoreDataProperties.h"
#import "MMCoreDataHandler.h"
#import "AppDelegate.h"

@interface MMStudentInformationViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic)NSManagedObjectContext *context ;
@property (nonatomic, strong) UITableView *table;
@property (retain, nonatomic)AppDelegate *appDelegate;
@property (retain, nonatomic)NSArray *resultArray;
@end

@implementation MMStudentInformationViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self. navigationItem.title = @"Details";
    
    self.resultArray= [[MMCoreDataHandler sharedManager] retrieveStudentDataFromStorage];
    
    self.table = [[UITableView alloc] init];
    self.table.translatesAutoresizingMaskIntoConstraints = NO;
    [self.table setBackgroundColor:[UIColor grayColor]];
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.view addSubview:self.table];
    
    NSDictionary *views = [NSDictionary dictionaryWithObjectsAndKeys:self.table, @"table", nil];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[table]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:views]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID = @"CellID";
    MMStudentTableViewCell *cell = [self.table dequeueReusableCellWithIdentifier:cellID];
    if(cell == nil)
    {
    cell = [[MMStudentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    if(self.resultArray.count > 0)
    {
        Student *item = [self.resultArray objectAtIndex:indexPath.row];
        cell.namelabel.text = [NSString stringWithFormat:@"%@",item.name];
        cell.emailID.text= [NSString stringWithFormat:@"%@",item.emailID];
        cell.contactLabel.text= [NSString stringWithFormat:@"%@",item.contactNo];
    }
    
    return  cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
