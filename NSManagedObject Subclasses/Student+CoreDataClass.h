//
//  Student+CoreDataClass.h
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Student : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Student+CoreDataProperties.h"
