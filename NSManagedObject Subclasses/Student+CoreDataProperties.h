//
//  Student+CoreDataProperties.h
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//
//

#import "Student+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *contactNo;
@property (nullable, nonatomic, copy) NSString *emailID;

@end

NS_ASSUME_NONNULL_END
