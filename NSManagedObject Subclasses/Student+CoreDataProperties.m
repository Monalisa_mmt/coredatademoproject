//
//  Student+CoreDataProperties.m
//  CoreDataDemoProject
//
//  Created by Monalisa on 22/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//
//

#import "Student+CoreDataProperties.h"

@implementation Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Student"];
}

@dynamic name;
@dynamic contactNo;
@dynamic emailID;

@end
