//
//  MMDataController.h
//  CoreDataDemoProject
//
//  Created by Monalisa on 23/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student+CoreDataProperties.h"
#import "AppDelegate.h"
#import "MMStudentViewController.h"


@interface MMCoreDataHandler : NSObject

+ (MMCoreDataHandler *)sharedManager;

@property (strong, nonatomic) NSArray *resultArray;
@property (strong, nonatomic)NSManagedObjectContext *context;
@property (retain, nonatomic)AppDelegate *appDelegate;
-(NSArray *)retrieveStudentDataFromStorage;
-(void)storeStudentDetails:(NSString *)name second:(NSString *)contactNo third:(NSString *)emailID;
@end
