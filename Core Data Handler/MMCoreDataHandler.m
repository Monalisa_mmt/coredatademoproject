//
//  MMDataController.m
//  CoreDataDemoProject
//
//  Created by Monalisa on 23/08/18.
//  Copyright © 2018 Media Magic Technologies. All rights reserved.
//

#import "MMCoreDataHandler.h"

@implementation MMCoreDataHandler

static MMCoreDataHandler *sharedCoreDataHandler = nil;

+( MMCoreDataHandler* )sharedManager
{
    static dispatch_once_t predicate = 0;
    dispatch_once(&predicate,^{
        
        sharedCoreDataHandler = [[self alloc] init];
        
    });
    return sharedCoreDataHandler;
}

-(id)init
{
    if( self == [super init] )
    {
        
    }
    return self;
}

-(void)storeStudentDetails:(NSString *)name second:(NSString *)contactNo third:(NSString *)emailID
{
    self.context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    NSManagedObject *studentObj = [NSEntityDescription insertNewObjectForEntityForName:@"Student" inManagedObjectContext:self.context];
    [studentObj setValue:name forKey:@"name"];
    [studentObj setValue:contactNo forKey:@"contactNo"];
    [studentObj setValue:emailID forKey:@"emailID"];
    NSFetchRequest<Student *> *fetchRequest = [Student fetchRequest];
    NSError *error;
    self.resultArray= [self.context executeFetchRequest:fetchRequest error:&error];
    [((AppDelegate*)[[UIApplication sharedApplication] delegate]) saveContext];
}
-(NSArray *)retrieveStudentDataFromStorage
{
    self.context = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;
    self.appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest<Student *> *fetchRequest = [Student fetchRequest];
    NSError *error ;
    self.resultArray= [self.context executeFetchRequest:fetchRequest error:&error];
    
    return self.resultArray;
}
@end
